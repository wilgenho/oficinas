package com.ma.comercial.oficinas.dominio;

import com.ma.comercial.shared.dominio.AggregateRoot;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Oficina extends AggregateRoot {

    private final Integer subunidad;
    private final String nombre;
    private final String direccion;
    private final String telefono;

    public Oficina(Integer subunidad, String nombre, String direccion, String telefono) {
        this.subunidad = subunidad;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public static Oficina crear(int subunidad, String nombre, String direccion, String telefono) {
        Oficina oficina = new Oficina(subunidad, nombre, direccion, telefono);
        oficina.record(OficinaCreada.fromDomain(oficina));
        return oficina;
    }

}

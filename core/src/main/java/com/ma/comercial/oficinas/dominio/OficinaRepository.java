package com.ma.comercial.oficinas.dominio;

import java.util.Optional;

public interface OficinaRepository {
    Optional<Oficina> consultar(int subunidad);
    void crear(Oficina oficina);
}

package com.ma.comercial.oficinas.dominio;

import com.ma.comercial.shared.dominio.eventos.DomainEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OficinaCreada extends DomainEvent {

    private final Integer subunidad;
    private final String nombre;
    private final String direccion;
    private final String telefono;

    public OficinaCreada(Integer subunidad, String nombre, String direccion, String telefono) {
        super(subunidad.toString());
        this.subunidad = subunidad;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public static OficinaCreada fromDomain(Oficina oficina) {
        return new OficinaCreada(oficina.getSubunidad(), oficina.getNombre(), oficina.getDireccion(), oficina.getTelefono());
    }

    @Override
    public String eventName() {
        return "oficina.creada";
    }
}

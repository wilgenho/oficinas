package com.ma.comercial.oficinas.aplicacion;

import com.ma.comercial.oficinas.dominio.Oficina;
import com.ma.comercial.oficinas.dominio.OficinaRepository;
import com.ma.comercial.shared.dominio.EntidadNoEncontrada;
import com.ma.comercial.shared.dominio.Subunidad;
import com.ma.comercial.shared.dominio.UserCase;

@UserCase
public class ConsultarOficina {

    public static final String OFICINA_NO_ENCONTRADA = "Oficina %s no encontrada";

    private final OficinaRepository oficinaRepository;

    public ConsultarOficina(final OficinaRepository oficinaRepository) {
        this.oficinaRepository = oficinaRepository;
    }

    public Oficina consultar(Subunidad subunidad) {
        return oficinaRepository.consultar(subunidad.getValue())
                .orElseThrow(() -> new EntidadNoEncontrada(String.format(OFICINA_NO_ENCONTRADA, subunidad)));
    }

}

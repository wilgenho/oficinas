package com.ma.comercial.oficinas.aplicacion;

import com.ma.comercial.oficinas.dominio.Oficina;
import com.ma.comercial.oficinas.dominio.OficinaRepository;
import com.ma.comercial.shared.dominio.UserCase;
import com.ma.comercial.shared.dominio.eventos.EventBus;
import lombok.extern.log4j.Log4j2;

import javax.transaction.Transactional;

@Log4j2
@UserCase
public class CrearOficina {

    private OficinaRepository oficinaRepository;
    private EventBus eventBus;

    public CrearOficina(OficinaRepository oficinaRepository, EventBus eventBus) {
        this.oficinaRepository = oficinaRepository;
        this.eventBus = eventBus;
    }

    @Transactional
    public void crear(int subunidad, String nombre, String direccion, String telefono) {
        Oficina oficina = Oficina.crear(subunidad, nombre, direccion, telefono);
        oficinaRepository.crear(oficina);
        eventBus.publish(oficina.pullDomainEvents());
        log.info("Oficina creada: subunidad: {}, nombre: {}", subunidad, nombre);
    }
}

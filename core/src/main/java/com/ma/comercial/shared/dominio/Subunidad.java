package com.ma.comercial.shared.dominio;

import lombok.Value;

@Value
public class Subunidad {

    Integer value;

    public Subunidad(Integer value) {
        if (value == null || value < 0 || value > 999) {
            throw new IllegalArgumentException("El valor de la subunidad es incorrecto");
        }
        this.value = value;
    }
}

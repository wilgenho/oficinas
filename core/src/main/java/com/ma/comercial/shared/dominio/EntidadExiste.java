package com.ma.comercial.shared.dominio;

public class EntidadExiste extends RuntimeException {
    public EntidadExiste(String message) {
        super(message);
    }
}

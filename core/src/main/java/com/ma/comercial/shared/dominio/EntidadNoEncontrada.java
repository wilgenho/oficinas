package com.ma.comercial.shared.dominio;


public class EntidadNoEncontrada extends RuntimeException {

    public EntidadNoEncontrada(String message) {
        super(message);
    }
}

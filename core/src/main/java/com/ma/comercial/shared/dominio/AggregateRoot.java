package com.ma.comercial.shared.dominio;

import com.ma.comercial.shared.dominio.eventos.DomainEvent;

import java.util.ArrayList;
import java.util.List;

public abstract class AggregateRoot {
    private final List<DomainEvent> domainEvents = new ArrayList<>();

    public final List<DomainEvent> pullDomainEvents() {
        List<DomainEvent> events = new ArrayList<>(domainEvents);
        domainEvents.clear();
        return events;
    }

    protected final void record(DomainEvent event) {
        domainEvents.add(event);
    }
}
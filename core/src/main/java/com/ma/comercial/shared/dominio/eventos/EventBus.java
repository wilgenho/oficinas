package com.ma.comercial.shared.dominio.eventos;

import java.util.List;

public interface EventBus {
    void publish(final List<DomainEvent> events);
}

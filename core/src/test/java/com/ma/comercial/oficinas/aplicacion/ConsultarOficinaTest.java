package com.ma.comercial.oficinas.aplicacion;

import com.ma.comercial.oficinas.dominio.Oficina;
import com.ma.comercial.oficinas.dominio.OficinaMother;
import com.ma.comercial.oficinas.dominio.OficinaRepository;
import com.ma.comercial.shared.dominio.EntidadNoEncontrada;
import com.ma.comercial.shared.dominio.Subunidad;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConsultarOficinaTest {

    private ConsultarOficina consultarOficina;

    @Mock
    private OficinaRepository oficinaRepository;

    @BeforeEach
    void init() {
        consultarOficina = new ConsultarOficina(oficinaRepository);
    }

    @Test
    void deberiaConsultarUnaOficina() {
        Oficina ejemplo = OficinaMother.ejemplo();
        Subunidad subunidad = new Subunidad(100);

        when(oficinaRepository.consultar(100)).thenReturn(Optional.of(ejemplo));

        Oficina oficina = consultarOficina.consultar(subunidad);
        assertThat(oficina, equalTo(ejemplo));
    }

    @Test
    void deberiaDarErrorSiNoExisteOficina() {
        when(oficinaRepository.consultar(999)).thenReturn(Optional.empty());
        Subunidad subunidad = new Subunidad(999);
        assertThrows(EntidadNoEncontrada.class, () -> {
            consultarOficina.consultar(subunidad);
        });
    }

}
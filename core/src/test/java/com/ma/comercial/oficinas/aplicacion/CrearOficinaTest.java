package com.ma.comercial.oficinas.aplicacion;

import com.ma.comercial.oficinas.dominio.Oficina;
import com.ma.comercial.oficinas.dominio.OficinaEventoMother;
import com.ma.comercial.oficinas.dominio.OficinaMother;
import com.ma.comercial.oficinas.dominio.OficinaRepository;
import com.ma.comercial.shared.dominio.eventos.DomainEvent;
import com.ma.comercial.shared.dominio.eventos.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CrearOficinaTest {

    @Captor
    private ArgumentCaptor<Oficina> oficinaCaptor;

    @Captor
    private ArgumentCaptor<List<DomainEvent>> eventosCaptor;

    private CrearOficina crearOficina;
    @Mock
    private OficinaRepository oficinaRepository;
    @Mock
    private EventBus eventBus;

    @BeforeEach
    void init() {
        crearOficina = new CrearOficina(oficinaRepository, eventBus);
    }

    @Test
    void deberiaCrearUnaOficina() {
        doNothing().when(oficinaRepository).crear(any());
        doNothing().when(eventBus).publish(any());

        crearOficina.crear(101, "Capital Federal",
                "Av. Belgrano 672", "(011) 4310-5400");


        verify(oficinaRepository, times(1)).crear(oficinaCaptor.capture());
        verify(eventBus, times(1)).publish(eventosCaptor.capture());
        assertThat(oficinaCaptor.getValue(), equalTo(OficinaMother.ejemplo()));
        assertThat(eventosCaptor.getValue(), equalTo(OficinaEventoMother.eventoCreada()));
    }

}
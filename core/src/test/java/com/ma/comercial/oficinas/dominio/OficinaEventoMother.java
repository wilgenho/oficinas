package com.ma.comercial.oficinas.dominio;

import com.ma.comercial.shared.dominio.eventos.DomainEvent;

import java.util.ArrayList;
import java.util.List;

public class OficinaEventoMother {
    private OficinaEventoMother() {}

    public static List<DomainEvent> eventoCreada() {
        List<DomainEvent> eventos = new ArrayList<>();
        eventos.add(new OficinaCreada(101, "Capital Federal",
                "Av. Belgrano 672", "(011) 4310-5400"));
        return eventos;
    }
}

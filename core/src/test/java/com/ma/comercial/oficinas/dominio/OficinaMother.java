package com.ma.comercial.oficinas.dominio;

import org.mockito.stubbing.Answer;

public class OficinaMother {
    public static Oficina ejemplo() {
        return new Oficina(101, "Capital Federal", "Av. Belgrano 672", "(011) 4310-5400");
    }
}

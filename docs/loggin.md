

## Depedendencias

Para formatear nuestros registros de log en JSON y puedan ser legibles por Fluentd podemos 
incluir la librería Logstash Logback Encoder en nuestras dependencias.

```xml
<dependency>
    <groupId>net.logstash.logback</groupId>
    <artifactId>logstash-logback-encoder</artifactId>
    <version>6.4</version>
</dependency>

```

## Configuración

Luego tenemos que crear un archivo logback-spring.xml para configurar la salida de logs.

Por defecto se muestran todos los log a partir del nivel INFO.

```xml
<configuration>
    <appender name="consoleAppender" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="net.logstash.logback.encoder.LogstashEncoder"/>
    </appender>
    <root level="INFO">
        <appender-ref ref="consoleAppender"/>
    </root>
</configuration>
```
package com.ma.comercial.oficinas.web;

import com.ma.comercial.oficinas.aplicacion.CrearOficina;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CrearOficinaController implements CrearOficinaApi {

    private final CrearOficina crearOficina;

    public CrearOficinaController(final CrearOficina crearOficina) {
        this.crearOficina = crearOficina;
    }

    @Override
    @PostMapping("/oficinas")
    @ResponseStatus(HttpStatus.CREATED)
    public void crear(@RequestBody OficinaRequest request) {
        crearOficina.crear(request.getSubunidad(), request.getNombre(),
                request.getDireccion(), request.getTelefono());
    }
}

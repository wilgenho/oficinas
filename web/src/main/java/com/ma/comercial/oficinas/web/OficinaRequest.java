package com.ma.comercial.oficinas.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Datos de una oficina comercial.")
public class OficinaRequest {
    @ApiModelProperty(value = "Número de subunidad operativa", example = "398")
    private Integer subunidad;
    @ApiModelProperty(value = "Nombre de la oficina", example= "Olavarria")
    private String nombre;
    @ApiModelProperty(value = "Dirección de la oficina", example = "Dorrego 321")
    private String direccion;
    @ApiModelProperty(value = "Teléfono", example = "(029814) 4545441")
    private String telefono;
}

package com.ma.comercial.oficinas.web;

import com.ma.comercial.oficinas.aplicacion.ConsultarOficina;
import com.ma.comercial.shared.dominio.Subunidad;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsultarOficinaController implements ConsultarOficinaApi {

    private ConsultarOficina consultarOficina;

    public ConsultarOficinaController(ConsultarOficina consultarOficina) {
        this.consultarOficina = consultarOficina;
    }

    @Override
    @GetMapping("/oficinas/{subunidad}")
    public OficinaResponse consultar(@PathVariable("subunidad") int subunidad) {
        return OficinaResponse.fromDomain(consultarOficina.consultar(new Subunidad(subunidad)));
    }
}

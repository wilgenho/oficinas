package com.ma.comercial.oficinas.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value="Servicios de oficinas o sucursales", tags = "oficinas")
public interface CrearOficinaApi {

    @ApiOperation(value = "Crear una oficina",
            notes = "Ingresar datos para crear una oficina comercial"
    )
    void crear(
            @ApiParam(name="oficina", value = "Datos de una oficina", required = true)
            OficinaRequest request);
}

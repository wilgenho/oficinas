package com.ma.comercial.oficinas.web;

import com.ma.comercial.oficinas.dominio.Oficina;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Datos de una oficina comercial.")
public class OficinaResponse {

    @ApiModelProperty(value = "Número de subunidad operativa", example = "398")
    private Integer subunidad;
    @ApiModelProperty(value = "Nombre de la oficina", example= "Olavarria")
    private String nombre;
    @ApiModelProperty(value = "Dirección de la oficina", example = "Dorrego 321")
    private String direccion;
    @ApiModelProperty(value = "Teléfono", example = "(029814) 4545441")
    private String telefono;

    public static OficinaResponse fromDomain(Oficina oficina) {
        OficinaResponse response = new OficinaResponse();
        response.subunidad = oficina.getSubunidad();
        response.nombre = oficina.getNombre();
        response.direccion = oficina.getDireccion();
        response.telefono = oficina.getTelefono();
        return response;
    }

}

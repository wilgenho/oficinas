package com.ma.comercial.oficinas.web;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

@Api(value="Servicios de oficinas o sucursales", tags = "oficinas")
public interface ConsultarOficinaApi {
    @ApiOperation(value = "Consultar una oficina por el número de subunidad",
            notes = "Retorna los datos de una oficina comercial. " +
                    "La búsqueda es por número de subunidad operativa"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retorna los datos de una oficina"),
            @ApiResponse(code = 404, message = "Oficina no existe"),
            @ApiResponse(code = 400, message = "Número de subunidad no válido")
    })
    OficinaResponse consultar(
            @ApiParam("Número de subunidad operativa.")
            @PathVariable("subunidad") int subunidad);
}

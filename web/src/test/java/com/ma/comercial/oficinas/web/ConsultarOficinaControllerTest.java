package com.ma.comercial.oficinas.web;

import com.intuit.karate.junit5.Karate;
import com.ma.comercial.shared.AcceptanceTest;
import org.springframework.test.context.jdbc.Sql;

class ConsultarOficinaControllerTest extends AcceptanceTest {

    @Karate.Test
    @Sql("/sql/oficinas.sql")
    Karate deberiaConsultarUnaOficina() {
            return Karate.run("classpath:karate/consultar-oficina.feature");
    }
}
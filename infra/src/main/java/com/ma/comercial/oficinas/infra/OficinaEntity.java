package com.ma.comercial.oficinas.infra;

import com.ma.comercial.oficinas.dominio.Oficina;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "oficina")
public class OficinaEntity {

    @Id
    private Integer subunidad;
    private String nombre;
    private String direccion;
    private String telefono;

    public static OficinaEntity fromDomain(Oficina oficina) {
        OficinaEntity entity = new OficinaEntity();
        entity.subunidad = oficina.getSubunidad();
        entity.nombre = oficina.getNombre();
        entity.direccion = oficina.getDireccion();
        entity.telefono = oficina.getTelefono();
        return entity;
    }

    public Oficina toDomain() {
        return new Oficina(subunidad, nombre, direccion, telefono);
    }

}

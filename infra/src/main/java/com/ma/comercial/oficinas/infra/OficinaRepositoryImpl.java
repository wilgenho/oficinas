package com.ma.comercial.oficinas.infra;

import com.ma.comercial.oficinas.dominio.Oficina;
import com.ma.comercial.oficinas.dominio.OficinaRepository;
import com.ma.comercial.shared.dominio.EntidadExiste;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Optional;

@Repository
@Log4j2
public class OficinaRepositoryImpl implements OficinaRepository {

    private final EntityManager entityManager;

    public OficinaRepositoryImpl(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Optional<Oficina> consultar(int subunidad) {
        log.debug("Consultar oficina de subunidad {}" , subunidad);
        OficinaEntity entity = entityManager.find(OficinaEntity.class, subunidad);
        log.debug("Datos obtenidos de la oficina {}", entity);
        return Optional.ofNullable(entity == null ? null: entity.toDomain());
    }

    @Override
    public void crear(@NonNull Oficina oficina) {
        if (existse(oficina.getSubunidad())) {
            log.warn("Ya existe oficina con subunidad " + oficina.getSubunidad());
            throw new EntidadExiste("Ya existe oficina creada de la subunidad " + oficina.getSubunidad());
        }
        OficinaEntity entity = OficinaEntity.fromDomain(oficina);
        log.debug("Guardar datos de la oficina {}", entity);
        entityManager.persist(entity);
        entityManager.flush();
    }

    private boolean existse(int subunidad) {
        Optional<Oficina> optional = consultar(subunidad);
        return optional.isPresent();
    }

}


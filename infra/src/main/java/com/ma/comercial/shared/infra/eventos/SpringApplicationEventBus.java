package com.ma.comercial.shared.infra.eventos;

import com.ma.comercial.shared.dominio.eventos.DomainEvent;
import com.ma.comercial.shared.dominio.eventos.EventBus;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class SpringApplicationEventBus implements EventBus {

    private final ApplicationEventPublisher publisher;

    public SpringApplicationEventBus(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void publish(List<DomainEvent> events) {
        events.forEach(this::publish);
    }

    private void publish(final DomainEvent event) {
        publisher.publishEvent(event);
    }
}

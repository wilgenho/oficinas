package com.ma.comercial.oficinas.infra;

import com.ma.comercial.TestingConfig;
import com.ma.comercial.oficinas.dominio.Oficina;
import com.ma.comercial.oficinas.dominio.OficinaRepository;
import com.ma.comercial.shared.dominio.EntidadExiste;
import com.ma.comercial.shared.infra.ContainerBaseTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import javax.transaction.Transactional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = TestingConfig.class)
@Tag("infra")
class OficinaRepositoryImplTest extends ContainerBaseTest {

    @Autowired
    private OficinaRepository oficinaRepository;


    @Sql("/sql/oficinas.sql")
    @Test
    void deberiaConsultarOficina() {
        Oficina oficina = oficinaRepository.consultar(100).get();
        assertThat(oficina.getNombre(), equalTo("Nombre"));
    }

    @Test @Transactional
    void deberiaCrearOficina() {
        Oficina oficina = new Oficina(101, "Oficina", "Direccion", "545454");
        oficinaRepository.crear(oficina);
    }

    @Test @Transactional
    void deberiaDarErrorSiYaExisteOficina() {
        Oficina oficina = new Oficina(101, "Oficina", "Direccion", "545454");
        oficinaRepository.crear(oficina);
        assertThrows(EntidadExiste.class, () -> {
            oficinaRepository.crear(oficina);
        });
    }




}